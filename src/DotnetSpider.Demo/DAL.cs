﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MySqlSugar;

namespace DotnetSpider.Demo
{
    public class DAL
    {
    }
    public class BaseRepository<T>   where T : class, new()


    {
        public void Add(T entity)
        {
            using (var db = DBContext.GetInstance())
            {
                db.Insert(entity);
            }
        }

        public void BatchAdd(List<T> entities)
        {
            using (var db = DBContext.GetInstance())
            {
                db.SqlBulkCopy<T>(entities);
            }
        }
        public void Add(List<T> entities)
        {
            using (var db = DBContext.GetInstance())
            {
                db.InsertRange(entities);
            }
        }

        public void Delete(Expression<Func<T, bool>> exp)
        {
            using (var db = DBContext.GetInstance())
            {
                db.Delete<T>(exp);
            }
        }

        public void Delete(T entity)
        {
            using (var db = DBContext.GetInstance())
            {
                db.Delete<T>(entity);
            }

        }

        public void Update(Expression<Func<T, bool>> identityExp, T entity)
        {
            using (var db = DBContext.GetInstance())
            {
                db.Update(entity, identityExp);
            }
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> exp = null)
        {
            using (var db = DBContext.GetInstance())
            {
                if (exp == null)
                {
                    return db.Queryable<T>().ToList().AsQueryable();
                }
                else
                {
                    return db.Queryable<T>().Where(exp).ToList().AsQueryable();
                }


            }
        }



        public IQueryable<T> PageFind(int pageindex = 1, int pagesize = 10, Func<T, T> orderby = null,
        Expression<Func<T, bool>> exp = null)
        {
            using (var db = DBContext.GetInstance())
            {
                return db.Queryable<T>().Where(exp).ToPageList(pageindex, pagesize).OrderBy(orderby).AsQueryable();
            }
        }

        public T FindSingle(Expression<Func<T, bool>> exp = null)
        {
            using (var db = DBContext.GetInstance())
            {
                return db.Queryable<T>().Where(exp).First();
            }
        }

        public int GetCount(Expression<Func<T, bool>> exp = null)
        {
            using (var db = DBContext.GetInstance())
            {
                if (exp == null)
                {
                    return db.Queryable<T>().Count();
                }
                else
                {
                    return db.Queryable<T>().Where(exp).Count();
                }


            }
        }

        public bool IsExist(Expression<Func<T, bool>> exp)
        {
            using (var db = DBContext.GetInstance())
            {
                return db.Queryable<T>().Any(exp);
            }
        }


        public void Update(T entity)
        {
            using (var db = DBContext.GetInstance())
            {
                db.Update(entity);
            }
        }

        public void Update(Expression<Func<T, bool>> where, Expression<Func<T, T>> entity)
        {
            using (var db = DBContext.GetInstance())
            {

                db.Update(entity, where);
            }
        }

        public void Update(string setvalue, Expression<Func<T, bool>> where, T entity)
        {
            using (var db = DBContext.GetInstance())
            {
                db.Update(setvalue, where, entity);
            }
        }

        public void MakeModels()
        {
            using (var db = DBContext.GetInstance())
            {
                db.ClassGenerating.CreateClassFiles(db, ("c:/Models"), "DistPersonManager.Domain.Models");
            }

        }
    }
    public class DBContext
    {

        private static readonly string reval = "server=localhost;user id=root;password=root;persistsecurityinfo=True;database=avmoo";

        public static string ConnectionString
        {
            get
            {

                return reval;
            }
        }
        public static SqlSugarClient GetInstance()
        {
            SqlSugarClient db = new SqlSugarClient(ConnectionString);
            db.IsEnableLogEvent = true;//Enable log events
            db.LogEventStarting = (sql, par) => { Console.WriteLine(sql + " " + par + "\r\n"); };
            return db;
        }
    }
}
